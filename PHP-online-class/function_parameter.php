<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function functionWithParameter($a=0,$b=0){
    return sumOfParameters([$a, $b])*sumOfParameters([$a, $b]).'<br>';
}

function sumOfParameters($arr = []){
    $result = 0;
    foreach($arr as $value){
        $result += $value;
    }
    
    return $result;
}

echo functionWithParameter(10, 5);

echo sumOfParameters([2,3,4,5,100,200,400]);

$test = 'functionWithParameter';

echo $test(5,6);
